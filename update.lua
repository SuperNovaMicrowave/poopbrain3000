-- pastebin: 7ixpebzs

function download(url, file)
    local content = http.get(url).readAll()
    if not content then
        error("Could not connect to website")
    end
    f = fs.open(file, "w")
    f.write(content)
    f.close()
end

files = {
    "startup.lua",
    "util.lua",
    "terminal.lua",
    "overlay.lua",
    "sentry.lua"
}

for k, v in pairs(files) do
    print("Updating " .. v)
    if fs.exists("/" .. v) then
        fs.delete("/" .. v)
    end
    download("https://gitlab.com/SuperNovaMicrowave/poopbrain3000/-/raw/master/" .. v, "/" .. v)
end
