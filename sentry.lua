-- wrap module manipulator
local manip = peripheral.wrap("back")

-- define status and setting vars
local sentry_power = .5
local sentry_status = ""

-- setup mob selection tables
local target_names = { "Creeper", "Zombie", "Skeleton", "Spider", "Slime", "Zombie Villager" }
local mob_lookup = {}
for i = 1, #target_names do
	mob_lookup[target_names[i]] = true
end


local function cartesian_to_polar(x, y, z)
    local pitch = -math.atan2(y, math.sqrt(x * x + z * z))
    local yaw = math.atan2(-x, z)

    return math.deg(pitch), math.deg(yaw)
end

local function cartesian_distance(x1, y1, z1, x2, y2, z2)
    local dx = x1 - x2
    local dy = y1 - y2
    local dz = z1 - z2
    local dist = math.sqrt(dx ^ 2 + dy ^ 2 + dz ^ 2)
    return dist
end

local function select_nearest_entity(entities)
    -- calculate distances of potential targets and store them in entity_by_dist
    local entity_by_dist = {}
    for i, entity in pairs(entities) do
        local dist = cartesian_distance(0, 0, 0, entity.x, entity.y, entity.z)
        entity_by_dist[dist] = entity
    end

    -- find closest entity
    local closest = 128
    for dist, entity in pairs(entity_by_dist) do
        if dist < closest then
            closest = dist
        end
    end

    return entity_by_dist[closest]
end

local function fire_at_entity(entity)
    local pitch, yaw = cartesian_to_polar(entity.x, entity.y, entity.z)
    manip.fire(yaw, pitch, sentry_power)
    -- wait after firing so that mobs have time to die
    os.sleep(.5)
end

function loop()
    while true do
        -- sense mobs in range
        local mobs = manip.sense()

        -- select targets
        local targets = {}
        for i = 1, #mobs do
            local mob = mobs[i]
            if mob_lookup[mob.name] then
                targets[#targets + 1] = mob
            end
        end

        -- if there is a target, fire at first target
        if #targets > 0 then
            local mob = select_nearest_entity(targets)
            fire_at_entity(mob)
        else
            -- if there are no mobs, it's unlikely that there will be some in under a second
            os.sleep(.5)
        end

        os.sleep(.05) -- prevent "too long with out yielding"
    end
end
