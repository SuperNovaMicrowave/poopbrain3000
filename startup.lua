-- load modules
os.loadAPI("util.lua")
os.loadAPI("terminal.lua")
os.loadAPI("overlay.lua")
os.loadAPI("sentry.lua")

-- trigger initial outputs
parallel.waitForAll(terminal.inital, overlay.inital)
-- run main loops
parallel.waitForAny(overlay.loop, sentry.loop)
